﻿using Microsoft.Extensions.Configuration;
using System;

namespace BareTalk.Exchange.Main.Extensions
{
    public static class ConfigurationExtractorExtension
    {
        public static T Extract<T>(this IConfigurationRoot configurationRoot)
        {
            var configurationName = typeof(T).Name;
            var value = configurationRoot
                .GetSection(configurationName)
                .Get<T>();

            if (value == null)
            {
                throw new ArgumentException($"Failed to extract configuration {configurationName}. Make sure that appsettings contains a section named \"{configurationName}\" and that the section contains properties whose names match {configurationName} object type.", configurationName);
            }

            return value;
        }
    }
}
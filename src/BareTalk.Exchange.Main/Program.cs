using BareTalk.Exchange.Logging;
using BareTalk.Exchange.Main.Middleware;
using BareTalk.Exchange.Messaging;
using BareTalk.Exchange.Signaling;
using BareTalk.Exchange.Signaling.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

const string Development = "Development";

var builder = WebApplication.CreateBuilder(args);

// register services
builder.Services.AddMessagingBus();
builder.Services.AddSignaling();
builder.Services.AddTransient<ErrorHandlingMiddleware>();

// configure host specifics
builder.Host.AddSerilog();

// configure pipeline
var app = builder.Build();
if (app.Environment.EnvironmentName != Development)
{
    app.UseHttpsRedirection();
}
app.UseAuthorization();
app.UseMiddleware<ErrorHandlingMiddleware>();
app.MapHub<MessagesExchangeHub>("/exchange");
app.Run();
﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Signaling.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BareTalk.Exchange.Signaling
{
    public static class DiExtension
    {
        public static IServiceCollection AddSignaling(this IServiceCollection services)
        {
            services.AddSignalR();
            services.AddTransient<IPushMessages, MessagePusher>();
            return services;
        }
    }
}
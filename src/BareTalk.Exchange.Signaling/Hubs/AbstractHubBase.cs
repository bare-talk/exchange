﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace BareTalk.Exchange.Signaling.Hubs
{
    public abstract class AbstractHubBase<T> : Hub<T> where T : class
    {
        protected readonly ILogger<AbstractHubBase<T>> logger;

        protected AbstractHubBase(ILogger<AbstractHubBase<T>> logger)
        {
            this.logger = logger;
        }
    }
}
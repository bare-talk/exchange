﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.ValueObjects;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BareTalk.Exchange.Signaling.Hubs
{
    public sealed class MessagesExchangeHub : AbstractHubBase<IClientMessageExchange>
    {
        private readonly IManageSubscribers subscribersManager;
        private readonly IMessageFactory messageFactory;
        private readonly IBusPublisher publisher;

        public MessagesExchangeHub(
            ILogger<AbstractHubBase<IClientMessageExchange>> logger,
            IManageSubscribers subscribersManager,
            IMessageFactory messageFactory,
            IBusPublisher publisher
        ) : base(logger)
        {
            this.subscribersManager = subscribersManager;
            this.messageFactory = messageFactory;
            this.publisher = publisher;
        }

        public override async Task OnConnectedAsync()
        {
            var username = Context
                .GetHttpContext()
                .Request
                .Headers["X-Username"];
            var usernameObject = new Username(username);

            var subscriber = subscribersManager.Add(usernameObject);
            if (subscriber == null)
            {
                await Clients.Caller.Error("Failed to start the message polling", 0);
                return;
            }

            await Groups.AddToGroupAsync(Context.ConnectionId, username);
            subscriber.Subscribe(usernameObject);
            await base.OnConnectedAsync();
            logger.LogInformation("{@User} has connected successfully.", username);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {

            var username = Context
               .GetHttpContext()
               .Request
               .Headers["X-Username"];
            var usernameObject = new Username(username);

            var poller = subscribersManager.Remove(usernameObject);
            if (poller != null)
            {
                logger.LogTrace("Attempting to stop the poller for {@User} as the user disconnected.", username);
                poller.Unsubscribe();
                logger.LogInformation("The poller for {@User} was {@Successfully} stopped.", username, "Successfully");
            }

            await base.OnDisconnectedAsync(exception);
            logger.LogInformation("{@User} has disconnected successfully.", username);
        }

        public async Task SendMessage(
            string jsonEncodedMessage,
            string from,
            string to,
            int messageTypeMarker
        )
        {
            logger.LogDebug("Got message from {@From} to {@To}", from, to);
            var genericMessage = messageFactory.Create(from, to, jsonEncodedMessage, messageTypeMarker);
            await publisher.PublishAsync(genericMessage);
        }
    }
}
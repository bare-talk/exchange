﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.ValueObjects;
using BareTalk.Exchange.Signaling.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BareTalk.Exchange.Signaling.Services
{
    internal sealed class MessagePusher : IPushMessages
    {
        private readonly IHubContext<MessagesExchangeHub, IClientMessageExchange> messagePushingHub;

        private readonly ILogger<MessagePusher> logger;

        public MessagePusher(
            IHubContext<MessagesExchangeHub, IClientMessageExchange> messagePushingHub,
            ILogger<MessagePusher> logger)
        {
            this.messagePushingHub = messagePushingHub;
            this.logger = logger;
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public async Task<bool> PushAsync(GenericMessage message)
        {
            logger.LogTrace("Attempting to push message from {@From} to {@To}", message.From.Username, message.To.Username);

            try
            {
                await messagePushingHub
                        .Clients
                        .Group(message.To.Username)
                        .ReceiveMessage(message.JsonEncodedPayload.JsonEncodedPayload, message.From.Username, message.MessageTypeMarker.MessageType);

                return true;
            }
            catch (Exception exception)
            {
                logger.LogError("Encountered error when pushing message to {@To}. Error: {@Error}", message.To.Username, exception.Message);
            }

            return false;
        }
    }
}
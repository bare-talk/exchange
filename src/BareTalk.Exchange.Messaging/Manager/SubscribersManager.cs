﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.ValueObjects;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;

namespace BareTalk.Exchange.Messaging.Manager
{
    internal sealed class SubscribersManager : IManageSubscribers
    {
        private readonly ILogger<SubscribersManager> logger;
        private readonly IServiceProvider serviceProvider;

        public SubscribersManager(ILogger<SubscribersManager> logger, IServiceProvider serviceProvider)
        {
            this.logger = logger;
            this.serviceProvider = serviceProvider;
        }

        private static readonly ConcurrentDictionary<string, IBusSubscriber> pollers = new();

        public IBusSubscriber? Add(Username username)
        {
            logger.LogTrace("Adding a new poller for {@User}", username);
            var subscriber = serviceProvider.GetService<IBusSubscriber>();
            if (subscriber == null)
            {
                return null;
            }

            var added = pollers.TryAdd(username.Value, subscriber);
            if (!added)
            {
                logger.LogWarning("Skipping poller registration, user already has poller.");
                return null;
            }

            logger.LogTrace("Successfully added a new poller for {@User}", username);
            return subscriber;
        }

        public IBusSubscriber? Remove(Username username)
        {
            logger.LogTrace("Removing poller for {@User}", username.Value);
            var removed = pollers.TryRemove(username.Value, out var poller);
            if (!removed)
            {
                logger.LogError("Failed to remove messages poller for user {@User}", username.Value);
                return null;
            }

            logger.LogTrace("Successfully removed poller for {@User}", username.Value);
            return poller;
        }
    }
}
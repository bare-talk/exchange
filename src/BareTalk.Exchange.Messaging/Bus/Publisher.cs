﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.ValueObjects;
using Microsoft.Extensions.Logging;
using RawRabbit;
using RawRabbit.Configuration.Exchange;
using System;
using System.Threading.Tasks;

namespace BareTalk.Exchange.Messaging.Bus
{
    internal sealed class Publisher : IBusPublisher
    {
        private readonly IBusClient busClient;
        private readonly ILogger<Publisher> logger;

        public Publisher(IBusClient busClient, ILogger<Publisher> logger)
        {
            this.busClient = busClient;
            this.logger = logger;
        }

        public async Task PublishAsync(GenericMessage message)
        {
            logger.LogTrace("Attempting to publish message.");

            try
            {
                await busClient.PublishAsync(message, configuration: config =>
                {
                    config.WithExchange(exchange =>
                    {
                        exchange.WithName(message.To.Username);
                        // exchange.WithType(ExchangeType.Direct);
                    });

                    config.WithRoutingKey(message.To.Username);
                });

                logger.LogTrace("Successfully published message.");
            }
            catch (Exception exception)
            {
                logger.LogError("Failed to publish message. Error: {@Error}", exception.Message);
            }
        }
    }
}
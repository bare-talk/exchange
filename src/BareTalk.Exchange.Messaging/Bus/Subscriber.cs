﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.ValueObjects;
using Microsoft.Extensions.Logging;
using RawRabbit;
using RawRabbit.Common;
using RawRabbit.Configuration.Exchange;

namespace BareTalk.Exchange.Messaging.Bus
{
    internal sealed class Subscriber : IBusSubscriber
    {
        private readonly IBusClient busClient;
        private readonly ILogger<Subscriber> logger;
        private readonly IPushMessages pushMessages;

        private ISubscription? subscription;

        public Subscriber(IBusClient busClient, ILogger<Subscriber> logger, IPushMessages pushMessages)
        {
            this.busClient = busClient;
            this.logger = logger;
            this.pushMessages = pushMessages;
        }

        public void Subscribe(Username username)
        {
            subscription = busClient.SubscribeAsync<GenericMessage>(async (message, _) =>
            {
                await pushMessages.PushAsync(message);
            }, configuration =>
            {
                configuration.WithExchange(ex =>
                {
                    ex.WithName(username.Value);
                    // ex.WithType(ExchangeType.Fanout);
                });

                configuration.WithRoutingKey(username.Value);
                configuration.WithSubscriberId(username.Value);
            });
        }

        public void Unsubscribe()
        {
            if (subscription != null)
            {
                logger.LogDebug("Attempting to unsubscribe consumer: {@Tag}", subscription.ConsumerTag);
                subscription.Dispose();
                logger.LogDebug("Successfully unsubscribed consumer: {@Tag}", subscription.ConsumerTag);
            }
        }
    }
}
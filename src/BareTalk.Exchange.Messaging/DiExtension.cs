﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.Factory;
using BareTalk.Exchange.Messaging.Bus;
using BareTalk.Exchange.Messaging.Manager;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RawRabbit.vNext;

namespace BareTalk.Exchange.Messaging
{
    public static class DiExtension
    {
        public static IServiceCollection AddMessagingBus(this IServiceCollection services)
        {
            return services
                .AddRawRabbit(cfg => cfg.AddJsonFile("rabbit.json"))
                .AddTransient<IBusPublisher, Publisher>()
                .AddTransient<IBusSubscriber, Subscriber>()
                .AddTransient<IManageSubscribers, SubscribersManager>()
                .AddTransient<IMessageFactory, MessageFactory>();
        }
    }
}
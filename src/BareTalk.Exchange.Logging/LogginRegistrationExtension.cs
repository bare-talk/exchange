﻿using Microsoft.Extensions.Hosting;
using Serilog;

namespace BareTalk.Exchange.Logging
{
    public static class LogginRegistrationExtension
    {
        public static IHostBuilder AddSerilog(this IHostBuilder builder)
        {
            return builder
               .UseSerilog((context, loggerConfiguration) =>
               {
                   loggerConfiguration.ReadFrom.Configuration(context.Configuration);
               });
        }

        public static IHostBuilder UseSerilogLogger(this IHostBuilder builder)
        {
            return builder.UseSerilog();
        }
    }
}
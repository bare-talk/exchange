﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class From
    {
        public readonly string Username;

        public From(string username)
        {
            Username = username;
        }
    }
}
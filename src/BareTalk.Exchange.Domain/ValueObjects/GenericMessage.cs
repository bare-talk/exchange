﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class GenericMessage
    {
        public GenericMessage(From from, To to, Payload jsonEncodedPayload, MessageTypeMarker messageTypeMarker)
        {
            From = from;
            To = to;
            JsonEncodedPayload = jsonEncodedPayload;
            MessageTypeMarker = messageTypeMarker;
        }

        public From From { get; }
        public To To { get; }
        public Payload JsonEncodedPayload { get; }
        public MessageTypeMarker MessageTypeMarker { get; }
    }
}
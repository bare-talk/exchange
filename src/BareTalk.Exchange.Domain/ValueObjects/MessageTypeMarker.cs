﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class MessageTypeMarker
    {
        public MessageTypeMarker(int messageType)
        {
            MessageType = messageType;
        }

        public int MessageType { get; }
    }
}
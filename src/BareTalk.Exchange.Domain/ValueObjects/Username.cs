﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class Username
    {
        public readonly string Value;

        public Username(string value)
        {
            Value = value;
        }
    }
}

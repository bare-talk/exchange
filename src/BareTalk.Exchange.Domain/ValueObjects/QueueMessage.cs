﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class QueueMessage
    {
        public QueueMessage(GenericMessage message, ulong deliveryTag)
        {
            Message = message;
            DeliveryTag = deliveryTag;
        }

        public GenericMessage Message { get; }
        public ulong DeliveryTag { get; }
    }
}
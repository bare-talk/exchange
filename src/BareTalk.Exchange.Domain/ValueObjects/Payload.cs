﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class Payload
    {
        public readonly string JsonEncodedPayload;

        public Payload(string jsonEncodedPayload)
        {
            JsonEncodedPayload = jsonEncodedPayload;
        }
    }
}
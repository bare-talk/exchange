﻿namespace BareTalk.Exchange.Domain.ValueObjects
{
    public sealed class To
    {
        public readonly string Username;

        public To(string username)
        {
            Username = username;
        }
    }
}
﻿using BareTalk.Exchange.Domain.ValueObjects;

namespace BareTalk.Exchange.Domain.Contracts
{
    public interface IManageSubscribers
    {
        IBusSubscriber? Add(Username username);

        IBusSubscriber? Remove(Username username);
    }
}
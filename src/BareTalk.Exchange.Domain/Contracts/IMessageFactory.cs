﻿using BareTalk.Exchange.Domain.ValueObjects;

namespace BareTalk.Exchange.Domain.Contracts
{
    public interface IMessageFactory
    {
        GenericMessage Create(string from, string to, string payload, int messageTypeMarker);
    }
}
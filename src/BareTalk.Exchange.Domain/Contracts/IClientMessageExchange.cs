﻿using System.Threading.Tasks;

namespace BareTalk.Exchange.Domain.Contracts
{
    public interface IClientMessageExchange
    {
        Task ReceiveMessage(string jsonEncodedMessage, string from, int messageType);

        Task Error(string errorMessage, int errorCode);
    }
}
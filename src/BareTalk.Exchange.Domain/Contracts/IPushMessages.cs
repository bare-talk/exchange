﻿using BareTalk.Exchange.Domain.ValueObjects;
using System.Threading;
using System.Threading.Tasks;

namespace BareTalk.Exchange.Domain.Contracts
{
    public interface IPushMessages
    {
        Task<bool> PushAsync(GenericMessage message);
    }
}
﻿using BareTalk.Exchange.Domain.ValueObjects;
using System.Threading.Tasks;

namespace BareTalk.Exchange.Domain.Contracts
{
    public interface IBusPublisher
    {
        Task PublishAsync(GenericMessage message);
    }
}
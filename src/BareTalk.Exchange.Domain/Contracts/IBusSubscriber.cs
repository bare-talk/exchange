﻿using BareTalk.Exchange.Domain.ValueObjects;

namespace BareTalk.Exchange.Domain.Contracts
{
    public interface IBusSubscriber
    {
        public void Subscribe(Username username);
        public void Unsubscribe();
    }
}
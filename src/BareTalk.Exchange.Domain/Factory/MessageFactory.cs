﻿using BareTalk.Exchange.Domain.Contracts;
using BareTalk.Exchange.Domain.ValueObjects;

namespace BareTalk.Exchange.Domain.Factory
{
    public class MessageFactory : IMessageFactory
    {
        public GenericMessage Create(string from, string to, string payload, int messageTypeMarker)
        {
            var fromObject = new From(from);
            var toObject = new To(to);
            var payloadObject = new Payload(payload);
            var messageTypemarkerObject = new MessageTypeMarker(messageTypeMarker);
            return new GenericMessage(fromObject, toObject, payloadObject, messageTypemarkerObject);
        }
    }
}